package ir.evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import ir.common.FileUtils;

public class Evaluation {
	
	public static final int MAX_NUMBER_OF_QUERIES_TO_PROCESS = 100; //max number of queries this engine would consider for evaluation purposes.
	
	private ArrayList<HashSet<String>> relevantList; //each index is for corresponding queryID, and has a set of relevant documents for that query
	
	public Evaluation(String relevantDataLocation){
		relevantList = FileUtils.loadRelevantData(relevantDataLocation);
	}

	public void computeStatistics(String searchEngineResultsLocation, String systemName, String outputLocation){
		
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(outputLocation+"/"+systemName+"_EVAL.csv"));
			writer.write("QueryId,Rank,DocID,Score,Precision,Recall\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		double avgPercisionSum = 0;
		double reciprocalRrankSum = 0;
		double percisionAt5Sum = 0;
		double percisionAt20Sum = 0;
		
		
		ArrayList<ArrayList<String>> queriesResultsArray = FileUtils.loadSearchEngineResults(searchEngineResultsLocation);
		//for MAP calculation maintain this count. we may miss some queries since they dont have rel. files.
		int totalNumberOfProcessedQueries = 0;
		HashSet<String> relDocs;
		for(int qID = 1; qID < queriesResultsArray.size(); qID++) {	
			relDocs = relevantList.get(qID);
			if(relDocs.isEmpty())
				continue;
			
			String[] resultsRows = new String[queriesResultsArray.get(qID).size()+1];
			double[] precisions = new double[queriesResultsArray.get(qID).size()+1];
			double[] recalls = new double[queriesResultsArray.get(qID).size()+1];
			double percisionSum = 0;
			double averagePercision = 0;
			
			int numberOfRelevantDocs = relDocs.size();
			
			int relevantRetrieved = 0;			
			boolean earlyRFound = false;
			
			for(int retrieved = 1; retrieved < queriesResultsArray.get(qID).size()+1; retrieved++){
				String resultsRecord = queriesResultsArray.get(qID).get(retrieved-1);
				String doc = resultsRecord.split(",")[2];
				if(relDocs.contains(doc)) {
					if(!earlyRFound){
						earlyRFound = true;
						reciprocalRrankSum += 1.0/(retrieved);
					}
					relevantRetrieved++;					
					percisionSum += ((double)relevantRetrieved)/retrieved;
				}	
				
				double precision = ((double)relevantRetrieved)/retrieved;
				double recall = ((double)relevantRetrieved)/numberOfRelevantDocs;
				precisions[retrieved] = precision;	
				recalls[retrieved] = recall;	
				resultsRows[retrieved] = resultsRecord;
			}
			if(relevantRetrieved == 0) 
				System.out.println("Check ID "+qID);
			else
				averagePercision = percisionSum/relevantRetrieved;
			
			avgPercisionSum += averagePercision;
			percisionAt5Sum += precisions[5];
			percisionAt20Sum += precisions[20];
			
			writePRTable(writer, qID, resultsRows, precisions, recalls, averagePercision);
			
			totalNumberOfProcessedQueries++;
		}
		double mRR = reciprocalRrankSum/totalNumberOfProcessedQueries;
		double mAP = avgPercisionSum/totalNumberOfProcessedQueries;
		double mPAt5 = percisionAt5Sum/totalNumberOfProcessedQueries;
		double mPAt20 = percisionAt20Sum/totalNumberOfProcessedQueries;
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("MRR: "+mRR);
		System.out.println("MAP: "+mAP);
		System.out.println("Mean P@5: "+mPAt5);
		System.out.println("Mean P@20: "+mPAt20);
		System.out.println("Results saved in: "+outputLocation+"/"+systemName+"_EVAL.csv");
		
	}
	
	private void writePRTable(BufferedWriter writer, int queryId, String[] resultsRows, double[] precision, double[] recall, double avp){		
		try {
			for(int i = 1; i < resultsRows.length; i++){
				writer.write(resultsRows[i]+","+precision[i]+","+recall[i]+"\n");						
			}	
			writer.write("AVERAGE PRECISION,"+avp+"\n");			
			writer.write("P@5,"+precision[5]+"\n");
			writer.write("P@20,"+precision[20]+"\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
