package ir;

import java.io.File;
import java.util.HashMap;

import ir.common.FileUtils;
import ir.evaluation.Evaluation;
import ir.search_engines.BM25SearchEngine;
import ir.search_engines.CosineSimSearchEngine;
import ir.search_engines.DefaultSearchEngine;
import ir.search_engines.SearchEngine;
import ir.search_engines.TfIdfSearchEngine;

public class Main {
	
	public enum MainCommand {SEARCHENGINE, EVAL};
	public enum Search_TASK_ID {T1, T2, T3, T4, T5, T6, T7, T8, T9};
	
	private static HashMap<String,String> searchEngineSystemNames;
	static {
		searchEngineSystemNames = new HashMap<String, String>();
		searchEngineSystemNames.put(Search_TASK_ID.T1.toString(), "T1.BM25");
		searchEngineSystemNames.put(Search_TASK_ID.T2.toString(), "T2.TFIDF");
		searchEngineSystemNames.put(Search_TASK_ID.T3.toString(), "T3.COSINE");
		searchEngineSystemNames.put(Search_TASK_ID.T4.toString(), "T4.LUCENE_DEFAULT");
		searchEngineSystemNames.put(Search_TASK_ID.T5.toString(), "T5.BM25.QEXPANSION");
		searchEngineSystemNames.put(Search_TASK_ID.T6.toString(), "T6.BM25.STOPPING");
		searchEngineSystemNames.put(Search_TASK_ID.T7.toString(), "T7.BM25.STEMMING");
		searchEngineSystemNames.put(Search_TASK_ID.T8.toString(), "T8.BM25.STOPPING.QEXPANSION");
		searchEngineSystemNames.put(Search_TASK_ID.T9.toString(), "T9.BM25.SNIPPET");
	}

	public static void main(String[] args) {
		if(args.length < 1) {
			System.err.println("USAGE: SEARCHENGINE ...");
			System.err.println("USAGE: EVAL ...");
		}
		
		String output = "OUTPUT";
		if(!new File(output).exists()) {
			new File(output).mkdir();
		}
		
		
		String mainCommand = args[0];
		
		if(mainCommand.equals(MainCommand.SEARCHENGINE.toString())) {
			if(args.length < 6) {
				System.err.println("USAGE: SEARCHENGINE T<TASK_NUM> <indexLocation> <corpusLocation> <queriesLocation> <stopwordsLocation>");
				System.exit(1);
			}
			String taskID = args[1];
			String indexLocation = args[2];
			String corpusLocation = args[3];
			String queriesLocation = args[4];
			String stopwordsLocation = args[5];
			SearchEngine engine = null;
			HashMap<Integer, String> queries = null;
			if(taskID.equals(Search_TASK_ID.T7.toString()))
				queries = FileUtils.loadStemmedQueries(queriesLocation);
			else
				queries = FileUtils.loadNonStemmedQueries(queriesLocation);
			
			if(taskID.equals(Search_TASK_ID.T1.toString())) {
				engine = new BM25SearchEngine(indexLocation, corpusLocation, stopwordsLocation);
			} else if(taskID.equals(Search_TASK_ID.T2.toString())) {
				engine = new TfIdfSearchEngine(indexLocation, corpusLocation, stopwordsLocation);
			} else if(taskID.equals(Search_TASK_ID.T3.toString())) {
				engine = new CosineSimSearchEngine(indexLocation, corpusLocation, stopwordsLocation);
			} else if(taskID.equals(Search_TASK_ID.T4.toString())) {
				engine = new DefaultSearchEngine(indexLocation, corpusLocation, stopwordsLocation);
			} else if(taskID.equals(Search_TASK_ID.T5.toString())) {
				engine = new BM25SearchEngine(indexLocation, corpusLocation, stopwordsLocation);
				engine.setQueryExpansionEnabled(true);
			} else if(taskID.equals(Search_TASK_ID.T6.toString())) {
				engine =  new BM25SearchEngine(indexLocation, corpusLocation, stopwordsLocation);
				engine.setStopwordRemovalEnabled(true);
			} else if(taskID.equals(Search_TASK_ID.T7.toString())) {
				engine =  new BM25SearchEngine(indexLocation, corpusLocation, stopwordsLocation);
				engine.setStemmingEnabled(true);
			} else if(taskID.equals(Search_TASK_ID.T8.toString())) {
				engine = new BM25SearchEngine(indexLocation, corpusLocation, stopwordsLocation);
				engine.setStopwordRemovalEnabled(true);
				engine.setQueryExpansionEnabled(true);
			}
			else if(taskID.equals(Search_TASK_ID.T9.toString())){
				engine = new BM25SearchEngine(indexLocation, corpusLocation, stopwordsLocation);
				engine.setSnippetEnabled(true);
			}
			
			if(engine != null) {
				engine.setSystemName(searchEngineSystemNames.get(taskID));
				engine.initialize();
				engine.saveBatchResults(queries, output+"/"+searchEngineSystemNames.get(taskID)+"_ENGINE.txt");
			}
			
		}
		
		else if(mainCommand.equals(MainCommand.EVAL.toString())) {
			
			if(args.length < 3) {	
				System.out.println("usage:>EVAL T<TASK_NUM> <RelevanceDataFileLocation>");
				System.exit(1);
			}
			
			String taskID = args[1];
			if(taskID.equals(Search_TASK_ID.T7.toString())) {
				System.err.println("No Evaluation for stemming version is available!");
				System.exit(0);
			}
			String relevanceDataFileLocation = args[2];
			String searchEngineResultsLocation = output+"/"+searchEngineSystemNames.get(taskID)+"_ENGINE.txt";
			
			Evaluation pr = new Evaluation(relevanceDataFileLocation);		
			pr.computeStatistics(searchEngineResultsLocation, searchEngineSystemNames.get(taskID), output);	
			
		} else {
			System.err.println("USAGE: SEARCHENGINE ...");
			System.err.println("USAGE: EVAL ...");
		}
		
	}
}
