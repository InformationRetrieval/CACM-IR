package ir.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ir.evaluation.Evaluation;
import ir.search_engines.SearchEngine;

public class FileUtils {
	
	public static HashMap<Integer, String> loadNonStemmedQueries(String queriesLocation) {
		HashMap<Integer, String> queryContainer = new HashMap<Integer, String>();
		try {
			org.jsoup.nodes.Document doc = Jsoup.parse(new File(queriesLocation), "UTF-8");
			Elements allElements = doc.getElementsByTag("DOC");
			for(Element e: allElements) {
				Integer qId = Integer.parseInt(e.getElementsByTag("DOCNO").first().ownText().trim());
				String queryString = e.ownText().trim();
				queryContainer.put(qId, queryString);
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.err.println("Could not load queries!");
			System.exit(1);
		}
		
		return queryContainer;
	}
	
	public static HashMap<Integer, String> loadStemmedQueries(String queriesLocation) {
		HashMap<Integer, String> queryContainer = new HashMap<Integer, String>();
		int i = 1;
		try {
			Scanner s = new Scanner(new File(queriesLocation));
			while(s.hasNextLine()) {
				queryContainer.put(i++, s.nextLine());
			}
			s.close();
		} catch(Exception e) {
			e.printStackTrace();
			System.err.println("Could not load queries!");
			System.exit(1);
		}
		
		return queryContainer;
	}
	
	public static HashMap<String,Integer> loadStopFilter(String stopwordsLocation){
		HashMap<String,Integer> smap = new HashMap<String, Integer>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(stopwordsLocation));
			String line = "";
			while((line = br.readLine())!=null){				
				smap.put(line, 1);
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open common words");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return smap;
	}
	
	public static ArrayList<ArrayList<String>> loadSearchEngineResults(String searchEngineResultsLocation){					
		ArrayList<ArrayList<String>> queriesResultsArray = new ArrayList<ArrayList<String>>();
		for(int i = 0; i < Evaluation.MAX_NUMBER_OF_QUERIES_TO_PROCESS+1; i++) {
			queriesResultsArray.add(new ArrayList<String>());
		}
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(new File(searchEngineResultsLocation)));				
			String line;
			int qID = 1;
			while((line = br.readLine())!=null && qID < Evaluation.MAX_NUMBER_OF_QUERIES_TO_PROCESS+1){				
				String[] ls = line.split("\\s+");
				String lineSummary = ls[0]+","+ls[3]+","+ls[2]+","+ls[4];
				qID = Integer.parseInt(ls[0]);
				//String doc = ls[2];				
				queriesResultsArray.get(qID).add(lineSummary);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		return queriesResultsArray;
	}
	
	public static ArrayList<HashSet<String>> loadRelevantData(String path) {
		ArrayList<HashSet<String>> relevantData = new ArrayList<HashSet<String>>();
		for(int i = 0; i < Evaluation.MAX_NUMBER_OF_QUERIES_TO_PROCESS+1; i++) {
			relevantData.add(new HashSet<String>());
		}	
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line = "";
			while((line = br.readLine())!=null){				
				String[] ls = line.split("\\s+");
				int qID = Integer.parseInt(ls[0]);				
				relevantData.get(qID).add(SearchEngine.fixDocID(ls[2]));
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open common words");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		return relevantData;	
	}
	
	public static void indexFileOrDirectory(String fileName, IndexWriter indexWriter)  {
		try {
			ArrayList<File> queue = new ArrayList<File>();
			addFiles(queue, new File(fileName));
	
			int originalNumDocs = indexWriter.numDocs();
			for (File f : queue) {
				FileReader fr = null;
				try {
					Document doc = new Document();
					fr = new FileReader(f);
					
					FieldType type = new FieldType();
					type.setIndexed(true);
					//type.setStored(true);
					type.setStoreTermVectors(true);
					//Field contents = new Field("contents", fr, type);
					doc.add(new Field("contents", fr, type));
					//doc.add(new TextField("contents", fr));
					doc.add(new StringField("path", f.getPath(), Field.Store.YES));
					doc.add(new StringField("filename", f.getName().replace(".html", ""),
							Field.Store.YES));
	
					indexWriter.addDocument(doc);
					System.out.println("Added: " + f);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Could not add: " + f);
					System.exit(0);
				} finally {
					fr.close();
				}
			}
	
			int newNumDocs = indexWriter.numDocs();
			System.out.println("");
			System.out.println("************************");
			System.out
			.println((newNumDocs - originalNumDocs) + " documents added.");
			System.out.println("************************");
	
			queue.clear();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Could not index the non-stemmed corpus");
			System.exit(1);
		}
	}

	private static void addFiles(ArrayList<File> queue, File file) {
		
		if (!file.exists()) {
			System.out.println(file + " does not exist.");
		}
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				addFiles(queue, f);
			}
		} else {
			String filename = file.getName().toLowerCase();
			if (filename.endsWith(".htm") || filename.endsWith(".html")
					|| filename.endsWith(".xml") || filename.endsWith(".txt")) {
				queue.add(file);
			} else {
				System.out.println("Skipped " + filename);
			}
		}
	}
	
	public static void indexCompositeFile(String fileName, IndexWriter indexWriter) {
		try {
			int originalNumDocs = indexWriter.numDocs();
			
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			
			StringBuilder docContent = new StringBuilder();
			String docId = "";
			String line = br.readLine();
			while(line != null) {
				if(line.startsWith("#"))
					docId = line.substring(2);
				else
					docContent.append(line+"\n");
					
				String nextLine = br.readLine();
				if(nextLine == null || nextLine.startsWith("#")) {
					Document doc = new Document();
					doc.add(new TextField("contents", docContent.toString(), Field.Store.YES));
					doc.add(new StringField("path", fileName+"#"+docId , Field.Store.YES));
					doc.add(new StringField("filename", "CACM-"+docId, Field.Store.YES));
					indexWriter.addDocument(doc);
					System.out.println("Added: " + "CACM-"+docId+".html");
					docContent = new StringBuilder();
				}
				
				line = nextLine;
	
			}
			br.close();
			
			int newNumDocs = indexWriter.numDocs();
			System.out.println("");
			System.out.println("************************");
			System.out
			.println((newNumDocs - originalNumDocs) + " documents added.");
			System.out.println("************************");
		} catch(IOException e) {
			e.printStackTrace();
			System.err.println("Could not index the stemmed corpus");
			System.exit(1);
		}
	}

	public static void delete(File file) {
		if(file.isDirectory()) {
			for(File sub: file.listFiles()) {
				delete(sub);
			}
			file.delete();
		}
		else {
			file.delete();
		}
		
	}
}
