package ir.query_expansion;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;

import ir.common.FileUtils;

public class QueryExpander {
	
	private FSDirectory indexFSDir;
	
	private HashMap<String,Integer> termMap;

	private HashMap<String,Integer> stopFilter;
	
	public QueryExpander(FSDirectory indexFSDir, String stopwordsLocation){
		this.indexFSDir = indexFSDir;
		termMap = new HashMap<String,Integer>();
		stopFilter = FileUtils.loadStopFilter(stopwordsLocation);
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
				.collect(Collectors.toMap(
						Map.Entry::getKey, 
						Map.Entry::getValue, 
						(e1, e2) -> e1, 
						LinkedHashMap::new
						));
	}

	public String expandQuery(String query, ScoreDoc[] hits, IndexSearcher searcher){				
		String expandedQuery = query;
		for (int k = 0; k < hits.length && k < 30; ++k) {					
			try {
				int docId = hits[k].doc;
				IndexReader indexReader = DirectoryReader.open(indexFSDir);
				TermsEnum terms = indexReader.getTermVector(docId,"contents").iterator(null);
			    while (terms.next() != null) {
			    	String term = terms.term().utf8ToString();
			    	int tfInDoc = terms.docFreq();
			    	if(stopFilter.containsKey(term) || term.length() <= 2 
							|| term.matches(".*[< >].*") || term.matches("[0-9]+")
							|| term.equals("cacm"))
						continue;
					if(!termMap.containsKey(term)){
						termMap.put(term, tfInDoc);
					}
					else{
						int val = termMap.get(term);
						termMap.put(term, val+tfInDoc);
					}
			    }			 
			}
			catch (IOException e) {
				e.printStackTrace();
			}			
		}
		HashMap<String,Integer> sortedMap = (HashMap<String, Integer>) sortByValue(termMap);
		int max = 0;
		String[] qs = query.split("\\s+");
		if(qs.length > 30)
			max = 0;
		else if(qs.length < 30 && qs.length > 15)
			max = 3;
		else
			max = 5;
		
		for(String t : sortedMap.keySet()){
			if(max <= 0)
				break;
			expandedQuery += " "+t;
			max--;
		}
		return expandedQuery;
	}

}
