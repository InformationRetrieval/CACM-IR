package ir.query_expansion;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.SortedMap;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.DocsEnum;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import ir.common.FileUtils;

public class QueryExpansion {
	
	public static final int MAX_TOP_TERM_NUM = 5;
	private FSDirectory indexFSDir;
	
	private PriorityQueue<TermFreq> topTerms = new PriorityQueue<TermFreq>(5, new Comparator<TermFreq>() {

		@Override
		public int compare(TermFreq o1, TermFreq o2) {
			return Integer.compare(o1.freq, o2.freq);
		}
	});
	
	private HashMap<String,Integer> stopFilter;
	
	public QueryExpansion(FSDirectory indexFSDir, String stopwordsLocation){
		this.indexFSDir = indexFSDir;
		stopFilter = FileUtils.loadStopFilter(stopwordsLocation);
	}

	public String expandQuery(String query, ScoreDoc[] hits, IndexSearcher searcher){				
		String expandedQuery = query;
		HashMap<String,Integer> termMap = new HashMap<String,Integer>();
		HashMap<String,Integer> sortedMap = new HashMap<String,Integer>();
		try {
			IndexReader indexReader = DirectoryReader.open(indexFSDir);
			HashSet<Integer> releventDocs = new HashSet<Integer>();
			HashSet<BytesRef> terms = new HashSet<BytesRef>();
			for (int k = 0; k < hits.length && k < 20 ; ++k) {
				int docId = hits[k].doc;
				releventDocs.add(docId);
				TermsEnum te = indexReader.getTermVector(docId,"contents").iterator(null);
				while (te.next() != null) {
					terms.add(te.term());
				}
			}
			
			for(BytesRef termRef: terms) {
				String term = termRef.utf8ToString();
				if(stopFilter.containsKey(term) || term.length() <= 2 
						|| term.matches(".*[< >].*") || term.matches("[0-9]+")
						|| term.equals("cacm")||term.equals("html")||term.equals("pre"))
					continue;
				DocsEnum de = MultiFields.getTermDocsEnum(indexReader, MultiFields.getLiveDocs(indexReader), "contents", termRef);
				int tfInRelevantSet = 0;
				int doc;
				while((doc = de.nextDoc()) !=  DocsEnum.NO_MORE_DOCS && releventDocs.contains(doc)) {
					tfInRelevantSet += de.freq();
				}
				if(!termMap.containsKey(term)){
					termMap.put(term, tfInRelevantSet);
				}
				else{
					int val = termMap.get(term);
					termMap.put(term, val+tfInRelevantSet);
				}
				addToTopTerms(new TermFreq(term, tfInRelevantSet));
			}
			sortedMap = (HashMap<String, Integer>) QueryExpander.sortByValue(termMap);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] qs = query.split("\\s+");
		if(qs.length > 30) {
			return expandedQuery;
		}		
		else if(qs.length < 30 && qs.length > 15){
			int i = 3;
			for(String key : sortedMap.keySet()){
				if(sortedMap.get(key) == 0)
					break;
				expandedQuery += " "+key;
				i--;
				if(i <= 0)
					break;
			}
		}
		else{
			int i = 5;
			for(String key : sortedMap.keySet()){
				if(sortedMap.get(key) == 0)
					break;
				expandedQuery += " "+key;				
				i--;
				if(i <= 0)
					break;
			}
		}		
		return expandedQuery;
	}
	
	private void addToTopTerms(TermFreq e) {
		if(topTerms.size() < MAX_TOP_TERM_NUM) {
			topTerms.add(e);
		} else {
			int minTopFreq = topTerms.peek().freq;
			if(minTopFreq < e.freq) {
				topTerms.poll();
				topTerms.add(e);
			}
		}
		
	}

	private class TermFreq {
		String term;
		int freq;
		TermFreq(String t, int f) {
			term = t;
			freq = f;
		}
	}

}
