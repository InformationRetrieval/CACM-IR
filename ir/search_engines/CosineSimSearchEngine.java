package ir.search_engines;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.BytesRef;

public class CosineSimSearchEngine extends SearchEngine {
	
	public CosineSimSearchEngine(String indexLocation, String corpusLocation, String stopwordsLocation) {
		super(indexLocation, corpusLocation, stopwordsLocation);
	}
	
	@Override
	protected Similarity getSimilarityMeasure() {
		return new CustomSimilarity();
	}

	class CustomSimilarity extends TFIDFSimilarity  {

		@Override
		public float coord(int overlap, int maxOverlap) {
			return 1f;
		}

		@Override
		public float queryNorm(float sumOfSquaredWeights) {
			return (float)Math.sqrt(sumOfSquaredWeights);
		}
		
		@Override
		public float tf(float freq) {
			return (float)(1+Math.log(freq));
		}
		
		@Override
		public float idf(long docFreq, long docCount) {
			return (float)Math.log(1+(docCount/(double)docFreq));
		}
		
		@Override
		public float lengthNorm(FieldInvertState state) {
			return 1f;
		}
		
		@Override
		public float decodeNormValue(long norm) {
			return (float)Double.longBitsToDouble(norm);
		}

		@Override
		public long encodeNormValue(float f) {
			return Double.doubleToLongBits(f);
		}

		@Override
		public float sloppyFreq(int distance) {
			return 0;
		}
		
		@Override
		public float scorePayload(int doc, int start, int end, BytesRef payload) {
			return 1F;
		}

	}
}
