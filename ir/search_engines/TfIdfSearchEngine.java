package ir.search_engines;

import org.apache.lucene.index.FieldInvertState;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.util.BytesRef;

public class TfIdfSearchEngine extends SearchEngine {
	
	public TfIdfSearchEngine(String indexLocation, String corpusLocation, String stopwordsLocation) {
		super(indexLocation, corpusLocation, stopwordsLocation);
	}
	
	@Override
	protected Similarity getSimilarityMeasure() {
		return new SimpleTFIDFSimilarity();
	}

}



class SimpleTFIDFSimilarity extends TFIDFSimilarity  {

	@Override
	public float coord(int overlap, int maxOverlap) {
		return 1f;
	}

	@Override
	public float queryNorm(float sumOfSquaredWeights) {
		return 1f;
	}
	
	@Override
	public float tf(float freq) {
		return freq;
	}
	
	@Override
	public float idf(long docFreq, long docCount) {
		return (float)Math.log(docCount/(double)(docFreq+1));
	}
	
	@Override
	public float lengthNorm(FieldInvertState state) {
		return 1f;
	}
	
	@Override
	public float decodeNormValue(long norm) {
		return (float)Double.longBitsToDouble(norm);
	}

	@Override
	public long encodeNormValue(float f) {
		return Double.doubleToLongBits(f);
	}

	@Override
	public float sloppyFreq(int distance) {
		return 0;
	}
	
	@Override
	public float scorePayload(int doc, int start, int end, BytesRef payload) {
		return 1F;
	}

}