package ir.search_engines;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import ir.common.FileUtils;
import ir.query_expansion.QueryExpansion;

public abstract class SearchEngine {
	
	public static final int MAX_NUMBER_OF_RESULTS = 100; //max number of docs this engine will return as a result for a query.
	
	public String systemName;
	
	protected Analyzer analyzer;
	
	protected IndexReader indexReader;
	protected IndexWriter indexWriter;
	private FSDirectory indexFSDir;
	
	private IndexSearcher searcher;
	
	private String indexLocation;
	private String corpusLocation;
	protected String stopwordsLocation;
	
	private boolean stopwordRemovalEnabled = false;
	private boolean snippetEnabled = false;
	public boolean isStopwordRemovalEnabled() {
		return stopwordRemovalEnabled;
	}
	public void setStopwordRemovalEnabled(boolean stopwordRemovalEnabled) {
		this.stopwordRemovalEnabled = stopwordRemovalEnabled;
	}	
	public boolean isSnippetEnabled(){
		return this.snippetEnabled;
	}
	private boolean queryExpansionEnabled = false;
	public boolean isQueryExpansionEnabled() {
		return queryExpansionEnabled;
	}
	public void setQueryExpansionEnabled(boolean queryExpansionEnabled) {
		this.queryExpansionEnabled = queryExpansionEnabled;
	}
	
	private boolean stemmingEnabled = false;
	public boolean isStemmingEnabled() {
		return stemmingEnabled;
	}
	public void setStemmingEnabled(boolean stemmingEnabled) {
		this.stemmingEnabled = stemmingEnabled;
	}
	public void setSnippetEnabled(boolean snippetEnabled) {
		this.snippetEnabled = snippetEnabled;
	}
	
	public void setSystemName(String name) {
		systemName = name;
	}
	
	public SearchEngine() {
		
	}
	
	public SearchEngine(String indexLocation, String corpusLocation, String stopwordsLocation) {
		this.indexLocation = indexLocation;
		this.corpusLocation = corpusLocation;
		this.stopwordsLocation = stopwordsLocation;
	
	}
	
	public void initialize() {
		initializeAnalyzer(); 
		initializeIndexer(indexLocation);
		createIndex(corpusLocation);
		initializeSearcher();
	}
	
	/*
	 * StopAnalyzer is SimpleAnalyzer+stop word removal
	 */
	public void initializeAnalyzer() {
		try {
			if(isStopwordRemovalEnabled())
				this.analyzer = new StopAnalyzer(Version.LUCENE_47, new BufferedReader(new FileReader(stopwordsLocation)));
			else
				this.analyzer = new SimpleAnalyzer(Version.LUCENE_47);
			
			
		} catch(Exception e) {
			e.printStackTrace();
			System.err.println("initializing analyzer failed!");
		}
	}
	
	protected abstract Similarity getSimilarityMeasure();
	
	public void initializeIndexer(String indexLocation) {
		File indexLoc = new File(indexLocation);
		if(indexLoc.exists())
			FileUtils.delete(indexLoc);
		indexLoc.mkdir();
		
		try {
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_47, analyzer);
			
			config.setSimilarity(getSimilarityMeasure());
			
		    indexFSDir = FSDirectory.open(new File(indexLocation));
			indexWriter = new IndexWriter(indexFSDir, config);
		} catch(IOException e) {
			e.printStackTrace();
			System.err.println("Intializing Indexer failed!");
			System.exit(1);
		}
	}
	
	
	public void createIndex(String corpusLocation) {
		try {
			if(isStemmingEnabled())
				FileUtils.indexCompositeFile(corpusLocation, indexWriter);
			else
				FileUtils.indexFileOrDirectory(corpusLocation, indexWriter);
			
			indexWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Creating Index failed!");
			System.exit(1);
		}
	}
	
	public void initializeSearcher() {
		try{
			indexReader = DirectoryReader.open(indexFSDir);
			searcher = new IndexSearcher(indexReader);
			searcher.setSimilarity(getSimilarityMeasure());
		} catch(IOException e) {
			e.printStackTrace();
			System.err.println("Intializing Searcher failed!");
			System.exit(1);
		}
	}
	public HashMap<String,Integer> generate_queryTermMap(String[] query){
		HashMap<String,Integer> qmap = new HashMap<String,Integer>();
		for(String term : query){
			qmap.put(term, 1);
		}
		return qmap;
	}
	public String[] split_doc(String path){
		String[] content = new String[]{"",""};		
		Pattern r = Pattern.compile("^[<].*[>]$");
		boolean headerFound = false;
		boolean descStartFound = false;
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line = "";
			while((line = br.readLine()) != null){
				Matcher m = r.matcher(line);
				line.trim();
				if(m.find() || line.equals(""))
					continue;
				if(line.startsWith("CACM"))
					break;
				if(!headerFound){
					headerFound = true;
					content[0] = line;
				}
				if(!descStartFound){
					content[1] += " "+line;
				}											
			}
			br.close();			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return content;
		
	}
	public String listToString(LinkedList<String> list){
		String line = "";		
		for(int i = 0; i < list.size(); i++){
			line += list.get(i)+" ";			
		}
		return line;
	}
	public String[] processed_term(String term){
		String pTerm = term.toLowerCase().replaceAll("[ \" \" \' \' \\. \\,]", "");		
		return pTerm.split("-");
	}
	
	public String[] process_description(String desc, HashMap<String,Integer> qmap){
		String snippet[] = new String[]{"",""};
		String[] desc_split = desc.split("\\s+");
		boolean initContentFound = false;
		LinkedList<String> snippetTermList = new LinkedList<String>();
		LinkedList<String> queryTermList = new LinkedList<String>();
		int termCount = 0, maxTermCount = -1;
		int count = 0;
		for(String term : desc_split){
			if(!initContentFound){
				snippetTermList.addLast(term);
				for(String t : processed_term(term)){
					if(qmap.containsKey(t)){
						termCount++;		
						queryTermList.addLast(t);
					}
				}
				if(count >= 40){
					initContentFound = true;
					snippet[0] = snippetTermList.toArray().toString();
					snippet[1] = queryTermList.toArray().toString();
				}
				else
					count++;				
			}
			else{				
				String t = snippetTermList.removeFirst();
				for(String pt : processed_term(term)){
					if(qmap.containsKey(pt)){
						termCount--;
						queryTermList.remove(pt);
					}
				}
				snippetTermList.addLast(term);
				if(qmap.containsKey(term)){
					termCount++;
					queryTermList.addLast(term);
					if(termCount > maxTermCount){
						maxTermCount = termCount;
						snippet[0] = listToString(snippetTermList);
						snippet[1] = listToString(queryTermList);
					}
				}								
			}
			if(maxTermCount == -1 && count < 20 && !snippetTermList.isEmpty()){
				snippet[0] = listToString(snippetTermList);
				if(!queryTermList.isEmpty())
					snippet[1] = listToString(queryTermList);
			}
		}
		return snippet;
	}
	
	public void write_snippet(String[] content, HashMap<String,Integer> qmap, BufferedWriter bw){		
		try {
			bw.write("<== Header ==>"+"\n"+content[0]+"\n");
			String[] snippet = process_description(content[1], qmap);
			bw.write("<== Desc ==>"+"\n"+snippet[0]+"\n"+"<=== Query Terms ===>"+"\n"+snippet[1]+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void generate_snippet(String query, String path, BufferedWriter writer){
		String[] qs = query.split("\\s+");
		HashMap<String,Integer> qmap = generate_queryTermMap(qs);
		String[] content = split_doc(path);
		write_snippet(content, qmap, writer);			
	}
	
	public ScoreDoc[] search(String queryString) {
		ScoreDoc[] hits = null;
		try {
			TopScoreDocCollector collector = TopScoreDocCollector.create(MAX_NUMBER_OF_RESULTS, true);
			QueryParser queryParser = new QueryParser(Version.LUCENE_47, "contents", analyzer);
			String escapedQueryString = QueryParser.escape(queryString);
			Query q = queryParser.parse(escapedQueryString);
			if(q == null){
				System.out.println("Query q is null");			
			}
			searcher.search(q, collector);
			hits = collector.topDocs().scoreDocs;
			if(isQueryExpansionEnabled()) {
				String expandedString = (new QueryExpansion(indexFSDir, stopwordsLocation)).expandQuery(escapedQueryString,hits,searcher);
				Query expQ = new QueryParser(Version.LUCENE_47, "contents", analyzer).parse(QueryParser.escape(expandedString));
				TopScoreDocCollector collectorQE = TopScoreDocCollector.create(MAX_NUMBER_OF_RESULTS, true);				
				searcher.search(expQ, collectorQE);				
				hits = collectorQE.topDocs().scoreDocs;
			}
			
		} catch(IOException e) {
			e.printStackTrace();
			System.err.println("failed to search: "+queryString);
			System.exit(1);
		} catch (ParseException e) {
			e.printStackTrace();
			System.err.println("failed to parse: "+queryString);
			System.exit(1);
		}
		return hits;
	}
	
	public void showResults(Integer queryId, ScoreDoc[] results) {
		try {
			for (int i = 0; i < results.length; ++i) {
				int docId = results[i].doc;
				Document d = searcher.doc(docId);
				System.out.println(queryId+" "+"Q0"+" "+" "+d.get("path")+" "+
				docId+" "+(i + 1)+ " " +results[i].score+" system");
			}
		} catch(IOException e) {
			e.printStackTrace();
			System.err.println("failed to show results for queryIDo: "+queryId);
		}
		
	}
	
	public void saveBatchResults(HashMap<Integer, String> queries, String outputPath) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath));
			for(Integer queryId: queries.keySet()) {
				String queryString = queries.get(queryId);
				
				System.out.println("searching for query "+queryId);
				ScoreDoc[] results = search(queryString);
				
				for (int i = 0; i < results.length; ++i) {
					int docId = results[i].doc;
					Document doc = searcher.doc(docId);
					writer.write(queryId+" "+"Q0"+" "+" "+doc.get("filename")+" "+(i + 1)+ " " +results[i].score+" "+systemName+"\n");
					if(isSnippetEnabled()){
						generate_snippet(queryString, doc.get("path"), writer);
						/*
						TermsEnum te = indexReader.getTermVector(docId,"contents").iterator(null);
						HashSet<BytesRef> terms = new HashSet<BytesRef>();
						while (te.next() != null) {
							terms.add(te.term());
							System.out.println(te.term().utf8ToString());
						}
						System.exit(9);
						*/
					}
				}
				
			} 
			System.out.println("Results saved in: OUTPUT/"+systemName+"_ENGINE.txt");
			writer.close();
		} catch(IOException e) {
				e.printStackTrace();
				System.err.println("failed to save results");
		} 
	}
	
	public static String fixDocID(String id){
		String docID = "";
		String[] ls = id.split("-");
		docID+=ls[0];
		if(ls[1].length() == 3)
			docID += "-0"+ls[1];
		else if(ls[1].length() == 2)
			docID += "-00"+ls[1];
		else if(ls[1].length() == 1)
			docID += "-000"+ls[1];
		else
			docID += "-"+ls[1];
		return docID;
	}
	
}

