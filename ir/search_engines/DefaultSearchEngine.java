package ir.search_engines;

import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.search.similarities.Similarity;

public class DefaultSearchEngine extends SearchEngine{

	public DefaultSearchEngine(String indexLocation, String corpusLocation, String stopwordsLocation) {
		super(indexLocation, corpusLocation, stopwordsLocation);
	}
	
	@Override
	protected Similarity getSimilarityMeasure() {
		return new DefaultSimilarity();
	}

}
