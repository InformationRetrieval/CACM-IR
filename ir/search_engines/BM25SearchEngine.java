package ir.search_engines;

import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;

public class BM25SearchEngine extends SearchEngine {
	
	public BM25SearchEngine(String indexLocation, String corpusLocation, String stopwordsLocation) {
		super(indexLocation, corpusLocation, stopwordsLocation);
	}
	
	@Override
	protected Similarity getSimilarityMeasure() {
		return new BM25Similarity();
	}

}